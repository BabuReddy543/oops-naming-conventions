package encapsulation;

import java.util.Scanner;
// Begining of the class
public class Addition2Numbers
{
// The entry of main() method
	public static void main(String[] args)
	{
           // Declaration of variables
		int a;
		int b;
		int c;
/*scanner class object to read values*/
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter first number");
	a=sc.nextInt();// Read next input as "int"
	System.out.println("Enter second number");
	b=sc.nextInt();// Read next input as "int"
	Addition2Numbers c=new Addition2Numbers();
	c=addition(a,b); // Add two numbers
// Display result
	System.out.println("Addition of two numbers is : "+c);
  }// Exit of main() method
}// End of the class



