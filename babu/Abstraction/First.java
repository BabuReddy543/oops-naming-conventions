package abstraction;
import java.util.*;
import java.util.Scanner;

// Beginning of class
public abstract class First
{
// Declare variables
	int a;
	int b;
	int sum;
// The input() method takes the values
	    abstract void input();
// The add() method adds two values
	    abstract void add();
// The result() method displays the result value
	    abstract void result();
/*scanner class object to read values*/
 Scanner sc = new Scanner(System.in);
}// End of the class
